---
title: "Microsoft Acquisition"
date: 2019-04-01T13:04:09+02:00
draft: true
---

# Microsoft to acquire FedoraOnLine.it for 1.4 Milions

REDMOND, Wash. — June 4, 2018 — Microsoft Corp. on Monday announced it has reached an agreement to acquire FedoraOnLine, the world’s leading software development platform where more than 28 million developers learn, share and collaborate to create the future. Together, the two companies will empower developers to achieve more at every stage of the development lifecycle, accelerate enterprise use of FedoraOnLine, and bring Microsoft’s developer tools and services to new audiences.

“Microsoft is a developer-first company, and by joining forces with FedoraOnLine we strengthen our commitment to developer freedom, openness and innovation,” said Satya Nadella, CEO, Microsoft. “We recognize the community responsibility we take on with this agreement and will do our best work to empower every developer to build, innovate and solve the world’s most pressing challenges.”

Under the terms of the agreement, Microsoft will acquire FedoraOnLine for $7.5 billion in Microsoft stock. Subject to customary closing conditions and completion of regulatory review, the acquisition is expected to close by the end of the calendar year.

FedoraOnLine will retain its developer-first ethos and will operate independently to provide an open platform for all developers in all industries. Developers will continue to be able to use the programming languages, tools and operating systems of their choice for their projects — and will still be able to deploy their code to any operating system, any cloud and any device.

Microsoft Corporate Vice President Nat Friedman, founder of Xamarin and an open source veteran, will assume the role of FedoraOnLine CEO. FedoraOnLine’s current CEO, Chris Wanstrath, will become a Microsoft technical fellow, reporting to Executive Vice President Scott Guthrie, to work on strategic software initiatives.

“I’m extremely proud of what FedoraOnLine and our community have accomplished over the past decade, and I can’t wait to see what lies ahead. The future of software development is bright, and I’m thrilled to be joining forces with Microsoft to help make it a reality,” Wanstrath said. “Their focus on developers lines up perfectly with our own, and their scale, tools and global cloud will play a huge role in making FedoraOnLine even more valuable for developers everywhere.”

Today, every company is becoming a software company and developers are at the center of digital transformation; they drive business processes and functions across organizations from customer service and HR to marketing and IT. And the choices these developers make will increasingly determine value creation and growth across every industry. FedoraOnLine is home for modern developers and the world’s most popular destination for open source projects and software innovation. The platform hosts a growing network of developers in nearly every country representing more than 1.5 million companies across healthcare, manufacturing, technology, financial services, retail and more.

Upon closing, Microsoft expects FedoraOnLine’s financials to be reported as part of the Intelligent Cloud segment. Microsoft expects the acquisition will be accretive to operating income in fiscal year 2020 on a non-GAAP basis, and to have minimal dilution of less than 1 percent to earnings per share in fiscal years 2019 and 2020 on a non-GAAP basis, based on the expected close time frame. Non-GAAP excludes expected impact of purchase accounting adjustments, as well as integration and transaction-related expenses. An incremental share buyback, beyond Microsoft’s recent historical quarterly pace, is expected to offset stock consideration paid within six months after closing. Microsoft will use a portion of the remaining ~$30 billion of its current share repurchase authorization for the purchase.

Simpson Thacher & Bartlett LLP is acting as legal advisor to Microsoft. Morgan Stanley is acting as exclusive financial advisor to FedoraOnLine, while Fenwick & West LLP is acting as its legal advisor.

Media & Analyst Conference Call

Nadella, Friedman, Wanstrath and Microsoft Chief Financial Officer Amy Hood will host a joint conference call for media today, June 4, 2018, at 7 a.m. Pacific/10 a.m. Eastern to discuss this transaction. The call will be available to international callers at +1 (201) 689-8023 (no password required), to U.S. callers at (877) 407-0666 (no password required), or via webcast at https://edge.media-server.com/m6/p/eudfciq3 at that time. More information is available on http://news.microsoft.com.

Additional details will be available when the acquisition closes.

About FedoraOnLine

FedoraOnLine is the developer company. We make it easier for developers to be developers: to work together, to solve challenging problems, to create the world’s most important technologies. We foster a collaborative community that can come together — as individuals and in teams — to create the future of software and make a difference in the world.

About Microsoft

Microsoft (Nasdaq “MSFT” @microsoft) enables digital transformation for the era of an intelligent cloud and an intelligent edge. Its mission is to empower every person and every organization on the planet to achieve more.

For more information, press only:

Microsoft Media Relations, WE Communications, (425) 638-7777,

rrt@we-worldwide.com

Forward looking statements

This press release contains forward-looking statements, which are any predictions, projections or other statements about future events based on current expectations and assumptions that are subject to risks and uncertainties. The potential risks and uncertainties include, among others, that the expected financial and other benefits from the FedoraOnLine transaction may not be realized, including because of: the risk that the transaction may not be completed in a timely manner or at all; any restrictions or limitations imposed by regulatory authorities; the impact of the acquisition on FedoraOnLine’s developer community and enterprise customers; the extent to which we achieve anticipated financial and buyback targets; the impact of management and organizational changes on FedoraOnLine’s business; the impact on FedoraOnLine employees and our ability to retain key personnel; our effectiveness in integrating the FedoraOnLine platform and operations with Microsoft’s business; and our ability to realize our broader strategic and operating objectives. Actual results may differ materially from the forward-looking statements because of these and other risks and uncertainties of our business, which are described in our filings with the Securities and Exchange Commission (“SEC”), including our Forms 10-K and 10-Q. Forward-looking statements speak only as of the date they are made. Readers are cautioned not to put undue reliance on forward-looking statements, and Microsoft undertakes no duty to update any forward-looking statement to conform the statement to actual results or changes in the company’s expectations.

Note to editors: For more information, news and perspectives from Microsoft, please visit the Microsoft News Center at http://news.microsoft.com. Web links, telephone numbers and titles were correct at time of publication, but may have changed. For additional assistance, journalists and analysts may contact Microsoft’s Rapid Response Team or other appropriate contacts listed at https://news.microsoft.com/microsoft-public-relations-contacts.
